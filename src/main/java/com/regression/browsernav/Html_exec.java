package com.regression.browsernav;

import org.openqa.selenium.htmlunit.HtmlUnitDriver;

public class Html_exec {

	public static void main(String[] args) {
		
		// Declaring and initialising the HtmlUnitWebDriver
        HtmlUnitDriver unitDriver = new HtmlUnitDriver();
        
        // open demo site webpage
        unitDriver.get("https://www.amazon.in/");
		
	//Print the title of the page
        System.out.println("Title of the page is -> " + unitDriver.getTitle());
        
     

	}

}
