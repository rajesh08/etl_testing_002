package com.function.testing;

import java.time.Duration;
import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.edge.EdgeDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.openqa.selenium.ElementClickInterceptedException;
import org.openqa.selenium.StaleElementReferenceException;

public class TC_Datepicker_01 {


	
	  public static void datepicker(String Currentdate) {
		  System.setProperty("webdriver.edge.driver",System.getProperty("user.dir") +"\\driver\\msedgedriver.exe");
			 WebDriver driver=new EdgeDriver();
			 driver.manage().window().maximize();
			 driver.get("https://www.path2usa.com/travel-companion/");
			 JavascriptExecutor js=(JavascriptExecutor)driver;
			 
			  WebDriverWait wait=new WebDriverWait(driver,Duration.ofMillis(30000));
	          WebElement from= wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//input[@id='form-field-travel_from']")));
	          		
	          from.sendKeys("Los Angeles International Airport - (LAX) - Los Angeles, CALIFORNIA, USA");
		      WebElement to = wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//input[@id='form-field-travel_to']")));
		      to.sendKeys("Chennai International Airport - (MAA) - Chennai, Tamil Nadu, IN");
		      WebElement dateform = wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//input[@id='form-field-travel_comp_date']")));
		      js.executeScript("arguments[0].scrollIntoView();",dateform);   
		      dateform.click();
		      List<WebElement> lis = driver.findElements(By.xpath("//div[@class='flatpickr-innerContainer']/div[1]/div[2]/div[1]/span"));
		         for (int i = 0; i < lis.size(); i++) {
	        	  if (lis.get(i).isEnabled()) {
	        		 // System.out.println("enabled");
	        		  String lis1=lis.get(i).getText();
	        	
					if (lis1.equalsIgnoreCase(Currentdate)){
						try {
	        			  lis.get(i).click();
						}
						catch(ElementClickInterceptedException e) {
							
						  e.printStackTrace();
							
						}
						catch(StaleElementReferenceException e) {
							e.printStackTrace();
							
						}
						catch(Exception e) {
							e.printStackTrace();
						}
					}
	        	
					
	        	  else {
	        		  System.out.println("disabled");
	        	  	}
	          	}
				}
	        
	  }
	  
    
	public static void main(String[] args) {
		
		// TODO Auto-generated method stub
		try {
		datepicker("24");
		}
		catch(StaleElementReferenceException e) {
			e.printStackTrace();
			
		}
	
 } 
	
}