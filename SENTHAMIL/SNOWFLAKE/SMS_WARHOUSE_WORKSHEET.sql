SELECT * FROM TABLE(VALIDATE(employee_tbl, JOB_ID=>'01b2962e-3201-0126-0000-000802eea135'));
select * from snowflake.account_usage.query_history;




// Day04 How to create a Snowflake Virtual Warehouse  Create a Warehouse with Object Properties
CREATE OR REPLACE RESOURCE MONITOR SMS_WAREHOUSE_MONITOR
  WITH CREDIT_QUOTA = 5000
  TRIGGERS ON 75 PERCENT DO NOTIFY
           ON 100 PERCENT DO SUSPEND
           ON 110 PERCENT DO SUSPEND_IMMEDIATE;

CREATE WAREHOUSE if not exists sms_warehouse
WAREHOUSE_TYPE = 'snowpark-optimized'
WAREHOUSE_SIZE = LARGE
MAX_CLUSTER_COUNT = 4
MIN_CLUSTER_COUNT = 1
SCALING_POLICY =  ECONOMY
AUTO_SUSPEND = 200
AUTO_RESUME = TRUE
INITIALLY_SUSPENDED = TRUE
RESOURCE_MONITOR = 'SMS_WAREHOUSE_MONITOR'
COMMENT = 'SMS WARE HOUSE IS STARTED'
ENABLE_QUERY_ACCELERATION = TRUE
QUERY_ACCELERATION_MAX_SCALE_FACTOR = 2;

/* MAX_CONCURRENCY_LEVEL = 10 
  STATEMENT_QUEUED_TIMEOUT_IN_SECONDS = 60  
  STATEMENT_TIMEOUT_IN_SECONDS = 3600 
*/

SHOW WAREHOUSES; 
DESC WAREHOUSE sms_warehouse;
DESCRIBE WAREHOUSE sms_warehouse;

// Day05 Creation of Snowflake Database Schema and Tables Table Structure and Micro partitions


use warehouse sms_warehouse;

use role accountadmin;


CREATE DATABASE SMS_DB
 comment = 'DATABSE CREATED IN SNOW';

SHOW DATABASES;

SELECT CURRENT_ROLE(),CURRENT_DATABASE();

CREATE SCHEMA SMS_SCHEMA COMMENT = 'THIS IS SMS SCHEMA';

SHOW SCHEMAS;

SELECT CURRENT_ROLE(),CURRENT_DATABASE(),CURRENT_SCHEMA();

USE DATABASE SMS_DB;
USE SCHEMA SMS_SCHEMA;

drop table first_table;

CREATE TABLE FIRST_Table(
num number,
num10 number(10,1),
decimal decimal(20,2),
numeric numeric(30,3),
int int,
integer integer
);


  desc table first_table;
  describe table sms_db.ssms_schema.first_table;

  select get_ddl('table','first_table');

  insert into first_table values(20,1000,20.222222,30.222,205,123456);

 insert into first_table values(120,1000,120.222222,130.222,1205,111123456),
                                (456,12544,124.2321,78899,1254,145789),
                                (46,1544,24.321,78899,254,5789);
 
select * from first_table;


create or replace table sms_text_table(id int autoincrement,v varchar,c char,s string ,t text  );


select * from sms_text_table;


insert into sms_text_table(v,c,s,t) values('m','r','t','y'),('m','r','t','y'),('f','w','r','t'),('f','w','r','t');

create table bool( b boolean,n number,s string );

select * from bool;

insert into bool values(false,1,'null');


create table dateandtime(d date default current_date(),t time default current_time(),ts timestamp default current_timestamp());

insert into dateandtime(d) values (current_date());
insert into dateandtime(d,ts) values (current_date,current_timestamp);
insert into dateandtime(d,ts) values (current_date(),current_timestamp());
insert into dateandtime(d,t,ts) values (current_date(),current_time(),current_timestamp());     // all give the same inputs

select * from dateandtime;

