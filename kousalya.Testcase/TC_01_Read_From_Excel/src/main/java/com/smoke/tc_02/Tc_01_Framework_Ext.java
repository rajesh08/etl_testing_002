package com.smoke.tc_02;

import java.io.FileInputStream;

import java.io.IOException;
import java.time.Duration;
import java.util.Iterator;
import java.util.NoSuchElementException;

import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.xssf.usermodel.XSSFCell;
import org.apache.poi.xssf.usermodel.XSSFRow;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.edge.EdgeDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import com.relevantcodes.extentreports.ExtentReports;
import com.relevantcodes.extentreports.ExtentTest;
import com.relevantcodes.extentreports.LogStatus;

public class Tc_01_Framework_Ext {
	

	public static void main(String[] args) throws IOException, InterruptedException {
		

		ExtentReports reports =new ExtentReports(System.getProperty("user.dir")+"\\html_reports.html");
		ExtentTest test= reports.startTest("Login Module", "validate the credentails for the login page");

		
		  System.setProperty("webdriver.edge.driver",System.getProperty("user.dir") +"\\driver\\msedgedriver.exe");
		  WebDriver driver=new EdgeDriver();
		  driver.manage().window().maximize();
		  driver.get("https://adactinhotelapp.com/");
		  FileInputStream fis = new FileInputStream(System.getProperty("user.dir")+"\\testdata\\Book1.xlsx");
          XSSFWorkbook workbook = new XSSFWorkbook(fis);
          XSSFSheet sheet =workbook.getSheetAt(0);
          for (int i = 1; i <=sheet.getLastRowNum(); i++) {
          XSSFRow row=sheet.getRow(i);
         
          XSSFCell USERNAME=row.getCell(0);
          String usrname=USERNAME.getStringCellValue();
          XSSFCell PASSWORD=row.getCell(1);
          String pwd=PASSWORD.getStringCellValue();
          WebDriverWait wait=new WebDriverWait(driver, Duration.ofMillis(30000));
          WebElement user= wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//input[@name='username']")));
          user.clear();
		  user.sendKeys(usrname);
		  WebElement pass = wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//input[@name='password']")));
		  pass.clear();
		  pass.sendKeys(pwd);
		  WebElement login = wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//input[@name='login']")));
		  login.click();
		   try {
			  String title = driver.getTitle();
			  test.log(LogStatus.INFO,"Navigate","Navigate to adactin page");
			  
			  if (title.equalsIgnoreCase("Adactin.com - Search Hotel")) {
				  
			 test.log(LogStatus.PASS, "Validated the page title in homepage");
			 WebElement logout = wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//a[text()='Logout']")));
			 logout.click();  
			 WebElement  log= wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//a[text()='Click here to login again']")));
			 log.click();
		     driver.navigate().refresh();
			 }else {
			 WebElement wrng_credential= wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//b[text()='Invalid Login details or Your Password might have expired. ']")));
			 String error_msg=wrng_credential.getText();
			 test.log(LogStatus.FAIL, "Page is Invalid");  	
		     test.log(LogStatus.FAIL, "Error message");
			 }
			   }
			   catch (NoSuchElementException e) {
				  e.printStackTrace();
				
			}}
                workbook.close();
                reports.endTest(test);
      	        reports.flush();
      	        driver.close();
         }
	   }
	



