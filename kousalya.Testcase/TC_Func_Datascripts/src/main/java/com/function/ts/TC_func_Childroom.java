package com.function.ts;

import java.time.Duration;
import java.util.ArrayList;
import java.util.List;

import org.apache.log4j.BasicConfigurator;
import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.edge.EdgeDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.support.ui.WebDriverWait;

public class TC_func_Childroom {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		Logger log=LogManager.getLogger(TC_Func_Location.class);
		BasicConfigurator.configure();
		System.setProperty("webdriver.edge.driver",System.getProperty("user.dir") +"\\driver\\msedgedriver.exe");
		WebDriver driver=new EdgeDriver();
		  driver.manage().window().maximize();
		  driver.get("https://adactinhotelapp.com/");
		  WebDriverWait wait=new WebDriverWait(driver, Duration.ofMillis(30000));
          WebElement user= wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//input[@name='username']")));
          user.sendKeys("kousalya97");
		  WebElement pass = wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//input[@name='password']")));
		  pass.sendKeys("kousalya");
		  WebElement login = wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//input[@name='login']")));
		  login.click();
		  Select select =new Select(driver.findElement(By.xpath("//select[@name='child_room']")));
		  List<WebElement> lis=select.getOptions();
		  ArrayList<String> str=new ArrayList<String>();
		  for (WebElement options : lis) {
			  log.info(options.getText());
			  str.add(options.getText());
		  }
		  for (int i = 0; i < str.size(); i++) {
			  if(str.get(i).equalsIgnoreCase(lis.get(i).getText())) { 
				  lis.get(i).click();
				log.info("Testcase pass");
				}
				  else {
			    log.info("Testcase Fail"); 
			    }
			 
			 
	}
             driver.quit();
}
}