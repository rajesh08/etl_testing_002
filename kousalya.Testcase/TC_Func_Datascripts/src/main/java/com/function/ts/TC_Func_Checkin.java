package com.function.ts;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.time.Duration;
import java.util.Calendar;
import java.util.Date;

import org.apache.log4j.BasicConfigurator;
import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.edge.EdgeDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.support.ui.WebDriverWait;

public class TC_Func_Checkin {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		Logger log=LogManager.getLogger(TC_Func_Checkout.class);
		BasicConfigurator.configure();
		System.setProperty("webdriver.edge.driver",System.getProperty("user.dir") +"\\driver\\msedgedriver.exe");
		WebDriver driver=new EdgeDriver();
		  driver.manage().window().maximize();
		  driver.get("https://adactinhotelapp.com/");
		  WebDriverWait wait=new WebDriverWait(driver, Duration.ofMillis(30000));
          WebElement user= wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//input[@name='username']")));
          user.
          sendKeys("kousalya97");
		  WebElement pass = wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//input[@name='password']")));
		  pass.sendKeys("kousalya");
		  WebElement login = wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//input[@name='login']")));
		  login.click();
		  WebElement select =driver.findElement(By.xpath("//input[@name='datepick_in']"));
		  select.clear();
		  SimpleDateFormat formatter = new SimpleDateFormat("dd/MM/yyyy");
	      Date date=new Date();
	      log.info(formatter.format(date));
		  select.sendKeys(formatter.format(date));
           driver.quit();
	}}