package com.util.automation;

import java.util.HashMap;
import java.util.Map;

public class TC_Replace_Elements {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		 HashMap<Integer,String> hm=new HashMap<Integer,String>();    
	      hm.put(100,"LOGIN");    
	      hm.put(101,"HOME");    
	      hm.put(102,"PAYMENT");   
	      System.out.println("Initial list of elements:");  
	     for(Map.Entry m:hm.entrySet())  
	     {  
	        System.out.println(m.getKey()+" "+m.getValue());   
	     }  
	     System.out.println("Updated list of elements:");  
	     hm.replace(100, "MENSFASHION");  
	     for(Map.Entry m:hm.entrySet())  
	     {  
	        System.out.println(m.getKey()+" "+m.getValue());   
	     }  
	     System.out.println("Updated list of elements:");  
	     hm.replace(101, "HOME", "ADD TO CART");  
	     for(Map.Entry m:hm.entrySet())  
	     {  
	        System.out.println(m.getKey()+" "+m.getValue());   
	     }   
	     System.out.println("Updated list of elements:");  
	     hm.replaceAll((k,v) -> "AMAZON");  
	     for(Map.Entry m:hm.entrySet())  
	     {  
	        System.out.println(m.getKey()+" "+m.getValue());   
	     }  
	 

	}

}
