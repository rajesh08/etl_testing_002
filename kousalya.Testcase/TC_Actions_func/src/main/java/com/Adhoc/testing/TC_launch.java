package com.Adhoc.testing;

import java.time.Duration;
import java.util.List;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import io.github.bonigarcia.wdm.WebDriverManager;

public class TC_launch {
	public static void main(String[] args) {
		
	
	ChromeOptions chromeoptions= new ChromeOptions();
	WebDriverManager.chromedriver().setup();
	WebDriver driver= new ChromeDriver(chromeoptions);
    driver.get("https://opensource-demo.orangehrmlive.com/");
	driver.manage().window().maximize();
	WebDriverWait wait=new WebDriverWait(driver, Duration.ofMillis(30000));
    WebElement user= wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//input[@name='username']")));
    user.sendKeys("Admin");
	WebElement password=driver.findElement(By.xpath("//input[@name='password']"));
	password.sendKeys("admin123");
	WebElement login=driver.findElement(By.xpath("//button[@type='submit']"));
	login.click();
	WebElement admin= wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("(//span[1])[1]")));
    admin.click();
    WebElement job= wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("(//span[@class='oxd-topbar-body-nav-tab-item'])[2]")));
    job.click();
    WebElement jobtitle= wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//a[text()='Job Titles']")));
    jobtitle.click();
    List<WebElement>checkboxes=driver.findElements(By.xpath("input[@type='checkbox']"));
    JavascriptExecutor js=(JavascriptExecutor)driver;
	js.executeScript("window.scrollBy(0,document.body.scrollHeight)");
     System.out.println("total no of checkboxes " +checkboxes.size());
	 try {
		
		  int totalcheckboxes=checkboxes.size(); 
		  for (int i = totalcheckboxes-21; i < totalcheckboxes; i++) {
		  
		  checkboxes.get(i).click(); }
	    }
	    catch (IndexOutOfBoundsException e) {
	    	e.printStackTrace();
	    }
	    }
}
