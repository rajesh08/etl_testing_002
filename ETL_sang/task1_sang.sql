--First 3 highest salary:
select * from
    (select distinct salary
    from hr.employees
    order by salary desc)
where rownum<=3;

--First 3 lowest salary:
select * from
    (select distinct salary
    from hr.employees
    order by salary asc)
where rownum<=3;

--2nd highest salary
select min(salary) as Second_High_salary from
(
    select distinct salary
    from hr.employees
    order by salary desc
)
where rownum<=2;

--2nd lowest salary
select max(salary) as Second_Low_salary from
(
    select distinct salary
    from hr.employees
    order by salary asc
)
where rownum<=2;
