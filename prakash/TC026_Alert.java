package com.cloudfire.loginmodule;

import java.io.File;
import java.io.IOException;

import org.openqa.selenium.Alert;
import org.openqa.selenium.By;
import org.openqa.selenium.Capabilities;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.Platform;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.edge.EdgeDriver;
import org.openqa.selenium.edge.EdgeOptions;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.firefox.FirefoxOptions;
import org.openqa.selenium.io.FileHandler;
import org.openqa.selenium.remote.RemoteWebDriver;
import org.apache.commons.*;

public class TC026_Alert {

	public static void main(String[] args) throws InterruptedException, IOException {
		// TODO Auto-generated method stub
		EdgeOptions opt=new EdgeOptions();
		opt.addArguments("incognito");
		opt.addArguments("start-maximized");
		System.setProperty("webdriver.edge.driver", System.getProperty("user.dir")+"\\src\\main\\resources\\Driver\\msedgedriver.exe");
		WebDriver driver=new EdgeDriver(opt);
		JavascriptExecutor js=(JavascriptExecutor)driver;
	//	js.executeScript("window.location='https://only-testing-blog.blogspot.com/2014/01/textbox.html");
		driver.get("https://only-testing-blog.blogspot.com/2014/01/textbox.html");
		
		TakesScreenshot s1=(TakesScreenshot)driver;
		File C=s1.getScreenshotAs(OutputType.FILE);
		FileHandler.copy(C,new File(System.getProperty("user.dir")+File.separator+"src\\main\\resources\\ScreenShot\\j"+System.currentTimeMillis()+".jpg"));
		System.out.println("Screenshot has been Taken\n");
		
		Capabilities cap=((RemoteWebDriver) driver).getCapabilities();
		String browser_name=cap.getBrowserName();
		String browser_version=cap.getBrowserVersion();
		Platform w=cap.getPlatformName();
		
		System.out.println("******************************************\n");
		System.out.println(browser_name+":::"+browser_version+"::::"+w);
		//driver.manage().window().fullscreen(); 
		String Title=driver.getTitle();
		System.out.println("******************************************\n");
		System.out.println("The Title of thePage is: " +Title);
		String check_state=js.executeScript("return document.readyState").toString();
		if(check_state.trim().equalsIgnoreCase("Complete"))
		{
			System.out.println("Page is Ready");
		}
	WebElement d=driver.findElement(By.partialLinkText("Older Post"));
	js.executeScript("arguments[0].scrollIntoView(true);",d);
	driver.findElement(By.cssSelector("input[value='Show Me Alert']")).click();
	Alert a=driver.switchTo().alert();
String txt_show=a.getText();
System.out.println("******************************************\n");
System.out.println("Text show as: "+txt_show );
a.accept();

driver.findElement(By.cssSelector("button[onclick='myFunction()']")).click();
Alert a1=driver.switchTo().alert();
String txt_show1=a1.getText();
System.out.println("**************************************");
System.out.println("Text show as: "+txt_show1 );
a1.dismiss();

Thread.sleep(2000);
WebElement s=driver.findElement(By.cssSelector("div[id='demo']"));
String txt=s.getText();
if(txt.trim().equalsIgnoreCase("You pressed OK!"))
{
	System.out.println("TEST PASS");
}

driver.findElement(By.cssSelector("button[onclick='myFunctionf()']")).click();
Alert promt_txt=driver.switchTo().alert();
String s11=promt_txt.getText();
System.out.println("Text is: "+s11);
promt_txt.sendKeys("This is john"); 

try { 
	Thread.sleep(3000); 

}catch(Exception e) {
	e.printStackTrace();
}
 promt_txt.accept();
 
 
 

	}

}
