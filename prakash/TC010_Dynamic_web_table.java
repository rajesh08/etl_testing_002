package com.cloudfire.loginmodule;

import java.time.Duration;
import java.util.ArrayList;
import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;

public class TC010_Dynamic_web_table {
public static WebDriver driver=null;
	public static void main(String[] args) {
		// TODO Auto-generated method stub
ChromeOptions options=new ChromeOptions();
options.addArguments("-incognito");
options.addArguments("--start-maximized");
System.setProperty("window.chrome.driver", System.getProperty("user.dir")+"\\src\\main\\resources\\Driver\\chromedriver.exe");
 driver=new ChromeDriver(options);
driver.navigate().to("https://money.rediff.com/gainers/bse/daily/groupa?src=gain_lose");
driver.manage().timeouts().implicitlyWait(Duration.ofSeconds(5));
List<WebElement> rows=driver.findElements(By.xpath("//table[@class='dataTable']//tbody/tr"));
List<WebElement> columns=driver.findElements(By.xpath("//table[@class='dataTable']//thead/tr/th"));
List<String> expect=new ArrayList<String>();
expect.add("UCO Bank");
expect.add("central Bank");
expect.add("Siemens Ltd.");

for(int row=1;row<=rows.size();row++)
{  
	for(int col=1;col<=1;col++)
	{
		WebElement f=driver.findElement(By.xpath("//table[@class='dataTable']//tbody/tr["+row+"]/td["+col+"]/a"));
		String text=f.getText();
		for(int i=0;i<expect.size();i++)
		{
		if( text.contains(expect.get(i)))
			System.out.println("TEST CASE PASS*****************************************"+text);
		else
			System.out.println("Test case fail-->"+text);
	}
}
	}
	}

}
