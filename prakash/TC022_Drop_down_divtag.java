package com.cloudfire.loginmodule;

import java.time.Duration;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import com.cloudfire.config.configuration;
import com.cloudfire.util.Util;

public class TC022_Drop_down_divtag {

	public static void main(String[] args) throws InterruptedException {
		// TODO Auto-generated method stub
ChromeOptions opt=new ChromeOptions();
opt.addArguments("incognito");
opt.addArguments("start-maximized");
System.setProperty("webdeiver.chrome.driver", System.getProperty("user.dir")+"\\src\\main\\resources\\Driver\\chromedriver.exe");
WebDriver driver=new ChromeDriver(opt);
//JavascriptExecutor js=(JavascriptExecutor)driver;
//js.executeScript("window.location='https://opensource-demo.orangehrmlive.com/;");
driver.navigate().to("https://opensource-demo.orangehrmlive.com/");
driver.manage().timeouts().implicitlyWait(Duration.ofSeconds(4));
driver.findElement(By.name("username")).sendKeys(configuration.username);
driver.findElement(By.name("password")).sendKeys(configuration.password);
driver.findElement(By.xpath("//div/button")).click(); 
driver.manage().timeouts().implicitlyWait(Duration.ofSeconds(3));
driver.findElement(By.xpath("(//ul[@class='oxd-main-menu']/li)[2]")).click();
Thread.sleep(4000);

 /* WebElement a=driver.findElement(By.xpath( "(//div[@class='oxd-select-wrapper']/div[1]/div)[1]")); a.click();
  Thread.sleep(1000);
  driver.findElement(By.xpath("//*[text()='Part-Time Contract']")).click();
  WebElement a1=driver.findElement(By.xpath("(//div[@class='oxd-select-wrapper']/div[1]/div)[1]")); a.click();
  Thread.sleep(1000);
  driver.findElement(By.xpath("//*[text()='Freelance']")).click();
  WebElement a2=driver.findElement(By.xpath( "(//div[@class='oxd-select-wrapper']/div[1]/div)[1]")); a.click();
  Thread.sleep(1000);
  driver.findElement(By.xpath("//*[text()='Full-Time Permanent']")).click();*/


  Util o=new Util();
  o.drop_down("(//div[@class='oxd-select-wrapper']/div[1]/div)[1]","Part-Time Contract", driver);
  o.drop_down("(//div[@class='oxd-select-wrapper']/div[1]/div)[1]","Full-Time Probation", driver);
  o.drop_down("(//div[@class='oxd-select-wrapper']/div[1]/div)[1]","Freelance", driver);
  o.drop_down("(//div[@class='oxd-select-wrapper']/div[1]/div)[1]","Full-Time Permanent", driver);

  
  /*Util o1=new Util();
o1.status_drop_down("//div[@class='oxd-select-wrapper']/div)[4]", "Engineering", driver);
o1.status_drop_down("//div[@class='oxd-select-wrapper']/div)[4]", "OrangeHRM", driver);
o1.status_drop_down("//div[@class='oxd-select-wrapper']/div)[4]", "Development", driver);
o1.status_drop_down("//div[@class='oxd-select-wrapper']/div)[position()=4]", "Quality Assurance", driver);
o1.status_drop_down("//div[@class='oxd-select-wrapper']/div)[position()=4]", "Sales & Marketing", driver);
o1.status_drop_down("//div[@class='oxd-select-wrapper']/div)[position()=4]", "Administration", driver);
o1.status_drop_down("//div[@class='oxd-select-wrapper']/div)[position()=4]", "Human Resources", driver);
o1.status_drop_down("//div[@class='oxd-select-wrapper']/div)[position()=4]", "Technical Support", driver);
o1.status_drop_down("//div[@class='oxd-select-wrapper']/div)[position()=4]", "Engineering", driver);*/
  
  driver.findElement(By.xpath("(//div[@class='oxd-select-wrapper']/div[1]/div)[7]")).click();
  Thread.sleep(1000);
  driver.findElement(By.xpath("//*[text()='Administration']")).click();

  driver.findElement(By.xpath("(//div[@class='oxd-select-wrapper']/div[1]/div)[7]")).click();
  driver.findElement(By.xpath("//*[text()='OrangeHRM']")).click();
 /* driver.findElement(By.xpath("(//div[@class='oxd-select-wrapper']/div[1]/div)[7]")).click();
  driver.findElement(By.xpath("//*[text()='Engineering']")).click();
  Thread.sleep(1000);
  driver.findElement(By.xpath("(//div[@class='oxd-select-wrapper']/div)[4]")).click();
  driver.findElement(By.xpath("//*[text()='Quality Assurance']")).click();
  Thread.sleep(1000);
  driver.findElement(By.xpath("(//div[@class='oxd-select-wrapper']/div)[4]")).click();
  driver.findElement(By.xpath("//*[text()='Sales & Marketing']")).click();
  Thread.sleep(1000);
  driver.findElement(By.xpath("(//div[@class='oxd-select-wrapper']/div)[4]")).click();
  driver.findElement(By.xpath("//*[text()='Administration']")).click();
  Thread.sleep(1000);
  driver.findElement(By.xpath("(//div[@class='oxd-select-wrapper']/div)[4]")).click();
  driver.findElement(By.xpath("//*[text()='Finance']")).click();
  Thread.sleep(1000);
  driver.findElement(By.xpath("(//div[@class='oxd-select-wrapper']/div)[4]")).click();
  driver.findElement(By.xpath("//*[text()='Human Resources']")).click();*/
   


	}

}
