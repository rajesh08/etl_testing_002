package com.util.automation;

import java.util.HashSet;

public class Hash_Remove {
	public static void main(String[] args) {
		
	
	  HashSet<String> al=new HashSet<String>();  
      al.add("success");  
      al.add("education");  
      al.add("focus");  
      al.add("innovation");  
      System.out.println(" initial elements: "+al);  
      al.remove("focus");  
      System.out.println("After invoking remove method: "+al);  
      HashSet<String> set1=new HashSet<String>();  
      set1.add("inspiration");  
      set1.add("creativity");  
      al.addAll(set1);  
      System.out.println("after add List: "+al);  
      
      al.removeAll(set1);  
      System.out.println("After removeAll() method: "+al);  
        
      al.removeIf(str->str.contains("innovation"));    
      System.out.println("After removeIf() method: "+al);  
       
      al.clear();  
      System.out.println("After clear() method: "+al);  
}
}  

	 


