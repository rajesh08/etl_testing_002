package com.cloudfire.testbench;

import org.openqa.selenium.By;


import com.config.utils.Util;

public class ExtentReportSample extends Util {
	
	

	public static void main(String[] args) throws InterruptedException {

		Util.chromeBrowserLaunch("window.location='https://opensource-demo.orangehrmlive.com/web/index.php/auth/login'");
		Util.setExtent();
		titleTest();
		imgTest();
		//Util.endReport();
		driver.close();

	}
	public static void titleTest()
	{
		//test = extent.createTest("OrangeHRM Title Test");
		//test.log(Status.INFO, "Login Successful");
		String title = driver.getTitle();
		//test.log(Status.INFO, "Title is: "+title);
		String expectedTitle = "OrangeHRM";
		//test.log(Status.INFO, "Expected Title is: "+expectedTitle);
		//if(title.equals(expectedTitle))
			//test.log(Status.PASS, "Test Case Pass");
		//else
			//test.log(Status.FAIL, "Test Case Fail");
	}
	public static void imgTest() throws InterruptedException
	{
		//test = extent.createTest("OrangeHRM Image Test");
		Thread.sleep(6000);
		boolean imgVal = driver.findElement(By.xpath("(//img[@alt='orangehrm-logo'])[2]")).isDisplayed();
		System.out.println(imgVal);
		//if(imgVal)
			//test.log(Status.PASS, "Test case Pass and Image is available");
		////else
			//test.log(Status.FAIL, "Test case Fail and Image is not available");
	}

}
