package com.cloudfire.String;

public class StartsWithString {

	public static void main(String[] args) {
		String name = "Kannan";
		
		String prefix = "K";
		
		String pre = "kan";
		
		if(name.startsWith(prefix))
		{
			System.out.println("Valid");
		}
		else
			System.out.println("InValid");

		if(name.startsWith(prefix, 0))
			System.out.println("Valid Offset");
		else
			System.out.println("Invalid Offset");
	}

}
