package com.cloudfire.config;

public class Configuration {

	public static final String url = "https://opensource-demo.orangehrmlive.com/web/index.php/auth/login";
	public static final String onlyURL = "http://only-testing-blog.blogspot.com/2013/11/new-test.html";
	public static final String radiourl = "https://demoqa.com/automation-practice-form";
	public static final String uname = "username";
	public static final String pwd = "password";
	public static final String orgUname = "Admin";
	public static final String orgPwd = "admin123";
	public static final String homepage_title = "OrangeHRM";
	public static final String browsername = "GC";
	public static final String userdir = System.getProperty("user.dir");
	public static final String driver_path = "\\src\\main\\resources\\drivers\\";
		
}
