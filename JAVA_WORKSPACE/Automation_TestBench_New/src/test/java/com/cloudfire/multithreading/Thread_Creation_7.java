package com.cloudfire.multithreading;

public class Thread_Creation_7 extends Thread{
	
	public void run()
	
	{
		if(Thread.currentThread().isDaemon())
		{
			System.out.println("Daemon thread is working");
		}
		else
		{
			System.out.println("User thread is working");
		}
				
	}

	public static void main(String[] args) {
		Thread_Creation_7 th0 = new Thread_Creation_7();
		Thread_Creation_7 th1 = new Thread_Creation_7();
		Thread_Creation_7 th2 = new Thread_Creation_7();
		
		th0.setDaemon(true);
		th0.start();
		
		
		th1.start();
		th2.start();
		
	}

}
