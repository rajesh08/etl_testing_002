package com.cloudfire.arraylist;

import java.util.Collection;
import java.util.Collections;
import java.util.Iterator;
import java.util.Stack;
import java.util.Vector;

public class Stack1  {

	public static void main(String[] args) {
		Stack<String> stack = new Stack<String>();  
		stack.push("Mani");  
		stack.push("Mari");  
		stack.push("Madhesh");  
		stack.push("Kannan");  
		stack.push("Selva");  
		Iterator<String> itr=stack.iterator();  
		while(itr.hasNext()){  
		System.out.println(itr.next());  
		}  
	}

}
