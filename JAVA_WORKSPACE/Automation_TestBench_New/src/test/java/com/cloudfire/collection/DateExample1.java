package com.cloudfire.collection;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

public class DateExample1 {

	public static void main(String[] args) {
		Date date = new Date();
		SimpleDateFormat formatter = new SimpleDateFormat("dd/MM/yyyy hh:mm:ss:SSS");
		//SimpleDateFormat formatter = new SimpleDateFormat("dd/MM/yyyy");
		String currentdate = formatter.format(date);
		System.out.println("Current Date: "+currentdate);
		try {
			Date cdate = formatter.parse("20/03/2023 10:10:10:10");
			System.out.println("String to Date: "+cdate);
		} catch (ParseException e) {
			e.printStackTrace();
		}
		
		
	}

}
