package com.cloudfire.collection;

import java.util.LinkedHashSet;

public class LinkedHashSetExampleMain {

	public static void main(String[] args) {

		LinkedHashSet<Student> lhs = new LinkedHashSet<Student>();

		Student st1 = new Student(1, "Kannan", "MCA", 100, "PASS");
		Student st2 = new Student(2, "Naghul", "B.Tech(IT)", 100, "PASS");
		Student st3 = new Student(3, "RAM", "B.Tech(IT)", 100, "PASS");
		Student st4 = new Student(4, "SAI", "B.Tech(IT)", 100, "PASS");
		Student st5 = new Student(5, "MURUGAN", "B.Tech(IT)", 100, "PASS");
		Student st6 = new Student(5, "MURUGAN", "B.Tech(IT)", 100, "PASS");

		lhs.add(st1);
		lhs.add(st2);
		lhs.add(st3);
		lhs.add(st4);
		lhs.add(st5);
		lhs.add(st6);

		lhs.forEach((x) -> System.out
				.println(x.stu_id + " " + x.stu_name + " " + x.branch + " " + x.marks + " " + x.result));

	}

}
