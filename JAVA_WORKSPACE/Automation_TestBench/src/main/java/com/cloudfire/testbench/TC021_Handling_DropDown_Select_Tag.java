package com.cloudfire.testbench;


import java.util.List;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.support.ui.Select;

import com.cloudfire.config.Configuration;

public class TC021_Handling_DropDown_Select_Tag {

	public static void main(String[] args) {

		ChromeOptions option = new ChromeOptions();
		option.addArguments("-incognito");
		option.addArguments("--start-maximized");
		System.setProperty("webdriver.chrome.driver", Configuration.userdir +"\\src\\main\\resources\\drivers\\chromedriver.exe");
		WebDriver driver = new ChromeDriver(option);
		driver.get("https://demoqa.com/select-menu");
		//driver.manage().window().maximize();
		driver.manage().timeouts().implicitlyWait(3000, TimeUnit.MILLISECONDS);
		WebElement drop_down = driver.findElement(By.cssSelector("select#oldSelectMenu"));
		Select sel = new Select(drop_down);
		List<WebElement> options = sel.getOptions();		
		for (WebElement webElement : options) 
		{
			System.out.println("List of Options: "+webElement.getText());
		}
		//Select by Index
		sel.selectByIndex(1);
		System.out.println("Selected Option: "+sel.getFirstSelectedOption().getText());
		System.out.println("Tag Name: "+sel.getFirstSelectedOption().getTagName());
		//Select by Value
		sel.selectByValue("3");
		System.out.println("Selected Option: "+sel.getFirstSelectedOption().getText());
		System.out.println("Tag Name: "+sel.getFirstSelectedOption().getTagName());
		//Select by VisibleText
		sel.selectByVisibleText("Purple");
		System.out.println("Selected Option: "+sel.getFirstSelectedOption().getText());
		System.out.println("Tag Name: "+sel.getFirstSelectedOption().getTagName());
		//driver.close();
	}
}
