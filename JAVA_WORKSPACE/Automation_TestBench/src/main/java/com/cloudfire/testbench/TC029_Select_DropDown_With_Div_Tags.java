package com.cloudfire.testbench;

import java.util.List;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;

import com.cloudfire.config.Configuration;
import com.config.utils.Util;
public class TC029_Select_DropDown_With_Div_Tags {
	public static WebDriver driver = null;
	public static void main(String[] args) throws InterruptedException {
		ChromeOptions option = new ChromeOptions();
		option.addArguments("-incognito");
		option.addArguments("--start-maximized");
		System.setProperty("webdriver.chrome.driver", Configuration.userdir +"\\src\\main\\resources\\drivers\\chromedriver.exe");
		driver = new ChromeDriver(option);
		JavascriptExecutor js = (JavascriptExecutor)driver;
		//	js.executeScript("window.location='https://demoqa.com/select-menu'", "");
		js.executeScript("window.location='https://opensource-demo.orangehrmlive.com/web/index.php/auth/login'", "");
		//driver.get("https://demoqa.com/select-menu");
		//driver.manage().window().maximize();
		driver.manage().timeouts().implicitlyWait(3000, TimeUnit.MILLISECONDS);
		driver.findElement(By.name("username")).sendKeys("Admin");
		driver.findElement(By.name("password")).sendKeys("admin123");
		driver.findElement(By.xpath("//button[@type='submit']")).click();
		//driver.findElement(By.xpath("(//a)[3]//span[text()='PIM']")).click();
		List<WebElement> grid = driver.findElements(By.tagName("a"));
		for(int grid_index=2;grid_index<=2;grid_index++)
		{
			grid.get(grid_index).click();
		}

		Util obj = new Util();
		obj.dropDown("(//div[@class='oxd-select-wrapper']/div)[3]/div[1]", "Content Specialist",driver);
		//	Thread.sleep(5000);
		//driver.findElement(By.xpath("(//div/label[text()='Job Title']/following::div)[1]")).click();
		//driver.findElement(By.xpath("(//div/label[text()='Job Title']/following::div)[1]//*[text()='Account Assistant']")).click();
		//Thread.sleep(5000);
		//driver.findElement(By.xpath("(//div/label[text()='Job Title']/following::div)[1]")).click();
		//driver.findElement(By.xpath("(//div/label[text()='Job Title']/following::div)[1]//*[text()='Chief Executive Officer']")).click();
		//		driver.findElement(By.xpath("(//div/label[text()='Job Title']/following::div)[1]//*[text()='Chief Financial Officer']")).click();
		//	driver.findElement(By.xpath("(//div/label[text()='Job Title']/following::div)[1]//*[text()='Chief Technical Officer']")).click();
		//driver.findElement(By.xpath("(//div/label[text()='Job Title']/following::div)[1]//*[text()='Content Specialist']")).click();





	}

}
