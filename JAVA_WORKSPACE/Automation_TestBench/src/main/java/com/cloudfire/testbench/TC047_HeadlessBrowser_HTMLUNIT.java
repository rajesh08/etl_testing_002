package com.cloudfire.testbench;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.NoSuchElementException;

import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.htmlunit.HtmlUnitDriver;

import com.gargoylesoftware.htmlunit.BrowserVersion;

public class TC047_HeadlessBrowser_HTMLUNIT {

	public static void main(String[] args) {
		HtmlUnitDriver driver = new HtmlUnitDriver(BrowserVersion.CHROME);
		//HtmlUnitDriver driver = new HtmlUnitDriver(BrowserVersion.EDGE);
		//HtmlUnitDriver driver = new HtmlUnitDriver(BrowserVersion.FIREFOX);
		//driver.setJavascriptEnabled(true);
		driver.get("http://www.google.com");
		System.out.println(driver.getPageSource());
		System.out.println(driver.getTitle());
		System.out.println(driver.getCurrentUrl());
		System.out.println(driver.getBrowserVersion());
		System.out.println(driver.getCurrentUrl());
		WebElement ele = driver.findElement(By.xpath("//input[@name='q']"));
		ele.sendKeys("Htmlunit driver",Keys.ENTER);
		List<WebElement> lst = driver.findElements(By.tagName("a"));

		Iterator<WebElement> itr =lst.iterator();
		
		List<WebElement> arrlst = new ArrayList<WebElement>(lst);
		try {
			while(itr.hasNext())
			{
				System.out.println(itr.next().getText());
				arrlst.add(itr.next());
				
			}
			System.out.println("ArrayList::::::"+arrlst);
			System.out.println("ArrayList Size::::::"+arrlst.size());

			
		} catch (NoSuchElementException e) {
			e.printStackTrace();
		}
		catch(Exception e)
		{
			e.printStackTrace();
		}
			}

}
