package com.cloudfire.String;

public class StringAssign10 {

	public static void main(String[] args) {
		//10. Compare the given strings using compareTo() method:
		String s1="ABC";
		String s2="CAD";
		String s3="ABC";
		String s4="abc";
		String s5="hello";
		String s6="";
		String s7="me";
		System.out.println(s1.compareTo(s2));
		System.out.println(s1.compareTo(s3));
		System.out.println(s1.compareTo(s4));
		System.out.println(s5.compareTo(s6));
		System.out.println(s6.compareTo(s7));


	}
}

