package com.cloudfire.arrays;

public class Student_Main {
	
	static void min_value(int num[])
	{
		  num[0]=33;
		
		for (int i = 1; i < num.length; i++) {
			if(num[0]>num[i])
			{
				num[0] = num[i];
			}
				
		}
		System.out.println(num[0]);
	}
	

	public static void main(String[] args) {
		
		int num[] = new int[]{33,45,9,12,13};
		
		Student[] arr = new Student[5];
		
		arr[0] = new Student(3,"Kannan");
		arr[1] = new Student(6,"Kalai");
		arr[2] = new Student(9,"Naghul");
		arr[3] = new Student(10,"Tanishka");
		arr[4] = new Student(11,"Kalyani");
		
		for(int index=0;index<arr.length;index++)
		{
			System.out.println("Array Value-RollNo: "+arr[index].roll_no+"  Name: "+arr[index].name);
		}
		
		min_value(num);
	}

}
