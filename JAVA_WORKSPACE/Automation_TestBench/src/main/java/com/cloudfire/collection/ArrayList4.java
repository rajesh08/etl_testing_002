package com.cloudfire.collection;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Iterator;
import java.util.List;
import java.util.ListIterator;

public class ArrayList4 {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		 List<String> al = new ArrayList<String>(12);
		 System.out.println(al.size());
		 
		 al.add(0, "mango");
		 al.add(1,"banana");
		 al.add(2,"grapes");
		 al.add(3,"orange");
		
		 al.set(0,"kiwi");
		 
		 System.out.println(al);
		 
		 Iterator<String> itr =al.iterator();
		 while(itr.hasNext())
		 {
			 System.out.println("iterator: "+itr.next());
		 }
		 
		 ListIterator<String> lst=al.listIterator(al.size());
		 while(lst.hasPrevious())
		 {
			 System.out.println(lst.previous());
		 }
		 
		 Collections.sort(al);
		 System.out.println(al);

	}

}
