package com.cloudfire.healthcare;

public class ReverNumber2_WhileLoop {

	public static void main(String[] args) {
		int number = 123456789;
		int remainder;
		int reverse = 0;

		while (number!=0) 
		{

			remainder = number % 10;
			reverse = reverse*10 + remainder;
			number = number/10;
		}
		System.out.println("Reversed Number:==> "+reverse);
	}

}
