package com.cloudfire.healthcare;

public class DecisionMakingStatement3_ATF19 {
	public static void main(String[] args) {
		String dayname = "";
		byte day_of_the_week = 6;
		switch(day_of_the_week)
		{
		case 1: 
			dayname = "Sunday";
			System.out.println(dayname);
			break;
		case 2:
			dayname = "Monday";
			System.out.println(dayname);
			break;
		case 3:
			dayname = "Tuesday";
			System.out.println(dayname);
			break;
		case 4:
			dayname = "Wednesday";
			System.out.println(dayname);
			break;
		case 5:
			dayname = "Thursday";
			System.out.println(dayname);
			break;
		case 6:
			dayname = "Friday";
			System.out.println(dayname);
			break;
		case 7:
			dayname = "Saturday";
			System.out.println(dayname);
			break;
		default:
			System.out.println("Invalid day");
		}	
	}
}
