package com.cloudfire.healthcare;

public class WhileLoop1 {

	public static void main(String[] args) {
		int i = 1;

		while(i<=10)
		{
			System.out.println("i = "+i);
			i++;
		}
	}

}
