package com.cloudfire.healthcare;

public class SwitchCaseExample2 {

	public static void main(String[] args) {
		int yearofstudy = 5;
		char branchname = 'A';

		switch(yearofstudy)
		{
		case 1: 
			switch(branchname)
			{

			case 'C': 
				System.out.println("1Computer Science Engineering");
				break;
			case 'M':
				System.out.println("1MBBS");
				break;
			case 'A':
				System.out.println("1Auto Mobile");
				break;

			default:
				System.out.println("Default1");
			}
			break;
		case 2:
			switch(branchname)
			{

			case 'C': 
				System.out.println("2Computer Science Engineering");
				break;
			case 'M':
				System.out.println("2MBBS");
				break;
			case 'A':
				System.out.println("2Auto Mobile");
				break;

			default:
				System.out.println("Default2");
			}
			break;
		case 3:
			switch(branchname)
			{

			case 'C': 
				System.out.println("3Computer Science Engineering");
				break;
			case 'M':
				System.out.println("3MBBS");
				break;
			case 'A':
				System.out.println("3Auto Mobile");
				break;

			default:
				System.out.println("Default3");
			}
			break;

		case 4:
			switch(branchname)
			{

			case 'C': 
				System.out.println("4Computer Science Engineering");
				break;
			case 'M':
				System.out.println("4MBBS");
				break;
			case 'A':
				System.out.println("4Auto Mobile");
				break;

			default:
				System.out.println("Default4");
			}
			break;
		case 5:
			switch(branchname)
			{

			case 'C': 
				System.out.println("5Computer Science Engineering");
				break;
			case 'M':
				System.out.println("5MBBS");
				break;
			case 'A':
				System.out.println("5Auto Mobile");
				break;

			default:
				System.out.println("Default5");
			}
			break;
			default:
				System.out.println("Invalid");
		}
	}

}
