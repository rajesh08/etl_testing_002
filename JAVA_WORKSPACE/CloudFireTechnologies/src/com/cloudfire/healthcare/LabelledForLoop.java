package com.cloudfire.healthcare;

public class LabelledForLoop {

	public static void main(String[] args) {
		int i, j;

		out:
			for(i=0;i<=10;i++)
			{
				System.out.println("I value: "+i);
				if(i==5)
					break out;

				in:
					for(j=0;j<=10;j++)
					{
						if(j==5)
						{
							
							continue in;
						}
						else
						{
							System.out.println("J value: "+j);
						}
					}
			}




	}

}
