package com.cloudfire.patternprogram;

public class SamplePattern2 {

	public static void main(String[] args) {
		for(int no=5; no>=1;no--)
		{
		for(int i=1; i<=5; i++)
		{
			System.out.print(no+" ");
		}
		System.out.println();
		}
		
		//**********************
		System.out.println("@@@@@@@@@@@@@@@@");
		int row=5;
		while(row>=1)
		{
		for(int i=1; i<=5; i++)
		{
			System.out.print(row+" ");
		}
		System.out.println();row--;
		}

	}

}
