package com.cloudfire.patternprogram;

public class SamplePattern3 {

	public static void main(String[] args) {
		for(int row=5; row>=1;row--)
		{
		for(int col=1; col<=5; col++)
		{
			System.out.print(col+" ");
		}
		System.out.println();
		}
		
		//**********************
		System.out.println("@@@@@@@@@@@@@@@@");
		int row=5;
		int col;
		while(row>=1)
		{
		for(col=1; col<=5; col++)
		{
			System.out.print(col+" ");
		}
		System.out.println();row--;
		}

	}

}
