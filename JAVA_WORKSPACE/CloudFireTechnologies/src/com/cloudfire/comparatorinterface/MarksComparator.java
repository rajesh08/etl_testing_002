package com.cloudfire.comparatorinterface;
import java.util.*;


public class MarksComparator implements Comparator<Student>{

	@Override
	public int compare(Student o1, Student o2) {
		
		int m = o1.marks-o2.marks;
		return m;
		
	}

}
