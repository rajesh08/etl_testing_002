package com.cloudfire.comparatorinterface;

public class Student {
	String sname,schoolname;
	int marks;
	int age;
	public Student(String sname, String schoolname, int marks, int age) {
		super();
		this.sname = sname;
		this.schoolname = schoolname;
		this.marks = marks;
		this.age = age;
	}
	
	public String getSname() {
		return sname;
	}
	public void setSname(String sname) {
		this.sname = sname;
	}
	public String getSchoolname() {
		return schoolname;
	}
	public void setSchoolname(String schoolname) {
		this.schoolname = schoolname;
	}
	public int getMarks() {
		return marks;
	}
	public void setMarks(int marks) {
		this.marks = marks;
	}
	public int getAge()
	{
		return age;
	}
	public void setAge(int age)
	{
		this.age = age;
	}
	
	@Override
	public String toString() {
		return "Student [sname=" + sname + ", schoolname=" + schoolname + ", marks=" + marks + " age=" +age+ "]";
	}



}
