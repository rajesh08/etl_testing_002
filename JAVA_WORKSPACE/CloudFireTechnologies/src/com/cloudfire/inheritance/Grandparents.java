package com.cloudfire.inheritance;

public class Grandparents {
	String name;
	int land = 2; // acre
	int house = 1; // one house
	int conversion_from;
	String conversion_to;
	int value;
	
	/*
	 * protected Grandparents() {
	 * 
	 * }
	 */
	
	protected Grandparents(String name,int land,int house)
	{
		this.land = land;
		this.name = name;
		this.house = house;
		
	}
	protected int measurments(int conversion_from, String conversion_to)
	{
		
		if(conversion_to.equalsIgnoreCase("Cent"))
		{
			value = 1 * 100;
		}
		else if(conversion_to.equalsIgnoreCase("SquareFeet"))
		{
			value = 1 * 43360;
		}
		else
		{
			System.out.println("Invalid");
		}
		return value;
	}
}
