package com.cloudfire.interfaces;

public class EB_Main {

	public static void main(String[] args) {
		//EB_Calculation_Impl eb = new EB_Calculation_Impl();
		EB_Calculation eb = new EBCalIndustImpl();
		System.out.println("*****EB Bill for Domestic*****");
		System.out.println("For 100 Units: "+eb.domesticEBBill(100));
		System.out.println("For 150 Units: "+eb.domesticEBBill(150));
		System.out.println("For 200 Units: "+eb.domesticEBBill(200));
		System.out.println("For 250 Units: "+eb.domesticEBBill(250));
		System.out.println("For 300 Units: "+eb.domesticEBBill(300));
		System.out.println("For 350 Units: "+eb.domesticEBBill(350));
		System.out.println("For 400 Units: "+eb.domesticEBBill(400));
		System.out.println("For 450 Units: "+eb.domesticEBBill(450));
		System.out.println("For 500 Units: "+eb.domesticEBBill(500));
		System.out.println("For 501 Units: "+eb.domesticEBBill(501));
		System.out.println("For 600 Units: "+eb.domesticEBBill(600));
		System.out.println(); 
		System.out.println("*****Industries Flat Rate: 6.35/1 Unit*****");
		EBCalIndustImpl ei = new EBCalIndustImpl();
		System.out.println("For 100 Units: "+ei.IndustriesEBBill(100));
	}
}
