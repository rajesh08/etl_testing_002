package com.cloudfire.basicprograms;

public class Sample1 {
	
	public void bar(int x, String y) {
		
		String z = x+y;
	
	System.out.println(z);
	}

	public void bar(String y, int x) {
		
		String c = y+x;
		
	    System.out.println(c);
	}


	public static void main(String[] args) {
		
		
		Sample1 s1 = new Sample1();
		
		s1.bar(9, "Kannan");
		s1.bar("Murugan", 3);
		
		

	}

}
