package com.cloudfire.loginmodule;

import java.io.File;
import java.time.Duration;
import java.util.NoSuchElementException;

import javax.imageio.ImageIO;

import org.apache.log4j.Logger;
import org.apache.log4j.PropertyConfigurator;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.FluentWait;

import com.cloudfire.config.configuration;
import com.cloudfire.util.Reports;
import com.cloudfire.util.Util;
import com.relevantcodes.extentreports.ExtentReports;
import com.relevantcodes.extentreports.ExtentTest;

import ru.yandex.qatools.ashot.AShot;
import ru.yandex.qatools.ashot.Screenshot;
import ru.yandex.qatools.ashot.coordinates.WebDriverCoordsProvider;

public class Enabl_disable_box {
	public static Logger log = Logger.getLogger(Enabl_disable_box.class.getName());
	public static WebDriver driver = null;

	public static void main(String[] args) throws Exception {

		ExtentReports extent = Reports.report("Disable", true);
		ExtentTest test = Reports.pre_condition("Disable_Login", "PRAKASH", "SMOKE_TESTING");
		PropertyConfigurator.configure(
				System.getProperty("user.dir") + File.separator + "src\\main\\resources\\Properties\\Log4j.properties");

		WebDriver driver = Util.crossbrower(configuration.browser_name);
		driver.get(Util.read_property("url", "login"));
		// driver.navigate().to(configuration.url);
		log.info("Browser Launched Sucessfully");

		FluentWait<WebDriver> wait = new FluentWait<WebDriver>(driver).withTimeout(Duration.ofSeconds(60))
				.pollingEvery(Duration.ofSeconds(3)).ignoring(NoSuchElementException.class);
		WebElement User_name = null, p_word = null;

		driver.manage().timeouts().implicitlyWait(Duration.ofSeconds(4));
		try {
			User_name = wait.until(
					ExpectedConditions.presenceOfElementLocated(By.xpath(Util.read_property("name_txt", "login"))));

		} catch (NoSuchElementException e) {
			e.printStackTrace();
		} catch (Exception e) {
			e.printStackTrace();
		}
		User_name.clear();

		boolean t = User_name.isEnabled();
		if (t = true) {
			Reports.log_info("USER_NAME Validated");
			User_name.sendKeys(configuration.username);
		} else {
			Reports.log_info("USER_NAME Invalidated");
		}

		try {
			p_word = wait.until(
					ExpectedConditions.presenceOfElementLocated(By.xpath(Util.read_property("pwd_txt", "Login"))));

		} catch (NoSuchElementException e) {
			e.printStackTrace();
		} catch (Exception e) {
			e.printStackTrace();
		}
		p_word.clear();
		boolean p = p_word.isEnabled();
		if (p = true) {

			Reports.log_info("PASS_WORD Validated");

			p_word.sendKeys(configuration.password);
		} else {
			log.warn("Password mentioned wrongly");
			Reports.log_info("PASS_WORD Invalidated");

		}

		WebElement login = wait
				.until(ExpectedConditions.presenceOfElementLocated(By.xpath(Util.read_property("login_btn", "login"))));
		log.info("Login Button Present");
		JavascriptExecutor js = (JavascriptExecutor) driver;
		js.executeScript("arguments[0].setAttribute('style',arguments[1]);", login,
				"color:orange;border:9 px solid orange;");
		String disable = "arguments[0].setAttribute('disabled','true')";
		js.executeScript(disable, login);

		log.info("Login Button disabled");

		Reports.log_pass(test.addScreenCapture(Util.webelement_screenshot("login_disable", login)));
		Reports.log_info("Screenshot Captured Successfully ");
		Reports.post_condition();
	}
}
